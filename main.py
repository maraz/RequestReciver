import os
from flask import Flask, request, send_from_directory, url_for, redirect
from flaskext.mysql import MySQL



app = Flask(__name__, static_folder='static')

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'pass'
app.config['MYSQL_DATABASE_DB'] = 'myDB'
app.config['MYSQL_DATABASE_HOST'] = '172.17.0.2'
mysql.init_app(app)

UPLOAD_FOLDER = '/tmp/'
ALLOWED_EXTENSIONS = set(['txt'])

@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/Scenario1', methods=['GET', 'POST'])
def scenario1():
    if request.headers['Content-Type'] == "application/json" and str(request.args.get('parameter')) == "I'm a key!":
        con=mysql.connect()
        cursor=con.cursor()
        if request.method == 'GET' :
            cursor.execute("INSERT INTO records (RecordDate,RecordTime,RecordType) VALUE (current_date(),current_time(),'S GET')")
            con.commit()
            cursor.execute("SELECT * from records LIMIT 100")
            data = cursor.fetchone()
            return str(data)
        if request.method == 'POST':
            cursor.execute("INSERT INTO records (RecordDate,RecordTime,RecordType) VALUE (current_date(),current_time(),'S POST')")
            con.commit()
            return 'POST'
    return 'FAIL'

@app.route('/Scenario2', methods=['POST'])
def scenario2():
    con = mysql.connect()
    cursor = con.cursor()
    if request.headers['Content-Type'] == "application/json":
        params = request.get_json()
        if(len(params)==1):
            cursor.execute(
                "INSERT INTO records (RecordDate,RecordTime,RecordType) VALUE (current_date(),current_time(),'S2 JSON 1')")
            con.commit()
            return "JSON 1"
        if (len(params) == 1000):
            cursor.execute(
                "INSERT INTO records (RecordDate,RecordTime,RecordType) VALUE (current_date(),current_time(),'S2 JSON 1000')")
            con.commit()
            return "JSON 1000"
    if request.headers['Content-Type'] == "application/x-www-form-urlencoded":
        params = list(request.form.keys())
        if (len(params) == 1):
            cursor.execute(
                "INSERT INTO records (RecordDate,RecordTime,RecordType) VALUE (current_date(),current_time(),'S2 X-WWW 1')")
            con.commit()
            return "X-WWW 1"
        if (len(params) == 1000):
            cursor.execute(
                "INSERT INTO records (RecordDate,RecordTime,RecordType) VALUE (current_date(),current_time(),'S2 X-WWW 1000')")
            con.commit()
            return "X-WWW 1000"

@app.route('/Scenario3', methods=['GET', 'POST'])
def scenario3():
    full_url = url_for('scenario1', **request.args)
    return redirect(full_url)

@app.route('/Scenario4', methods=['GET', 'POST'])
def scenario4():
    full_url = url_for('scenario1', **request.args)
    return redirect(full_url)

@app.route('/Scenario5', methods=['GET', 'POST'])
def scenario5():
    full_url = url_for('scenario1', **request.args)
    return redirect(full_url)

@app.route('/Scenario6', methods=['GET', 'POST'])
def scenario6():
    full_url = url_for('scenario1', **request.args)
    return redirect(full_url)

@app.route('/Scenario7', methods=['GET', 'POST'])
def scenario7():
    full_url = url_for('scenario1', **request.args)
    return redirect(full_url)

@app.route('/Scenario8', methods=['GET', 'POST'])
def scenario8():
    full_url = url_for('scenario1', **request.args)
    return redirect(full_url)

@app.route('/Scenario9', methods=['GET'])
def scenario9():
    filenr=request.args.get('filenr')
    return send_from_directory('static', 's%s' % filenr)

@app.route('/Scenario10', methods=['POST'])
def scenario10():
    file = request.files['file']
    if file and allowed_file(file.filename):
        filename = file.filename
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return "OK"
    return "FAIL"

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

if __name__ == '__main__':
    app.run(host='0.0.0.0')


'''Stare śmieci
# Funkcja przygotowana do scenariusza pierwszego
# Sprawdza czy argumenty spełniają wymagania, jeżeli tak wykonuje proste działanie
@app.route('/Sce1', methods=['GET','POST'])
def Sce1():
    values = list(request.args.items())
    if request.method == 'GET':
        if len(values) == 1 and len(values[0][0]) <= 10 and len(values[0][1]) <= 10:
            return WorkGet()
        else:
            return "InCorrect GET"
    if request.method == 'POST':
        if len(values) == 1 and len(values[0][0]) <= 10 and len(values[0][1]) <= 10:
            WorkPost(SIZE)
            return "Correct POST"
        else:
            return "InCorrect POST"


@app.route('/Sce2', methods=['POST'])
def Sce2():
    WorkPost(SIZE)
    if request.headers['Content-Type'] == "application/json":
        params = request.get_json()
        return "JSON "+str(len(params))
    if request.headers['Content-Type'] == "application/x-www-form-urlencoded":
        params = list(request.form.keys())
        return "WWW "+str(len(params))


@app.route('/Sce9', methods=['GET'])
def Sce9():
    filenr=request.args.get('filenr')
    return send_from_directory('static', 's%s' % filenr)


@app.route('/Simple', methods=['GET','POST'])
def Simple():
    if request.method == 'GET':
        return WorkGet()
    if request.method == 'POST':
        WorkPost(SIZE)
        return "POST Recived"

#funkcja symulująca POST
def WorkPost(size):
    # list = [random.randrange(1, 101, 1) for _ in range(size)]
    print(sorted(list))

# Funkcja symulująca GET
def WorkGet():
        return app.open_resource('static/s%s' % FILENR).read()

'''

